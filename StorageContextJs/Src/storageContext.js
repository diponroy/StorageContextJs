﻿function StorageContext() {
    var self = this;
    
    self.storeKeys = [];
    
    self.storageSetting = function() {
        var
            keys = function () {
                throw new Error("keys setting for storage was not set");
            },
            set = function (storeKey, items) {
                throw new Error("set setting for storage was not set");
            },
            get = function(storeKey) {
                throw new Error("get setting for storage was not set");
            },
            drop = function(storeKey) {
                throw new Error("drop setting for storage was not set");
            },
            flash = function(storeKey) {
                throw new Error("flash setting for storage was not set");
            };
        return {
            keys: keys,
            set: set,
            get: get,
            drop: drop,
            flash: flash            
        };
    }();

    self.keys = function() {
        if (self.storeKeys.length === 0) {
            throw new Error("storeKeys to store data was not set");
        }
        return self.storeKeys;
    };

    self.mappToStoreKeys = function() {
        var keys = self.keys();
        for (var propName in keys) {
            self[propName] = new StorageSet(keys[propName], self.storageSetting);
        }
    };

    self.flash = function() {
        var keys = self.keys();
        for (var propName in keys) {
            self[propName].flash();
        }
    };
    
    self.create = function () {
        self.mappToStoreKeys();
        self.flash();
    };
    
    self.drop = function() {
        var keys = self.keys();
        for (var propName in keys) {
            self[propName].drop();
        }
    };
    
    self.dropAndCreate = function () {
        self.mappToStoreKeys();
        self.flash();
    };
}

var storageContext = new StorageContext();