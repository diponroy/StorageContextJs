﻿function QuerySet(items) {
    var self = this;
    self.items = items;
}

QuerySet.prototype.asAdded = function() {
    var own = this;
    for (var i = 0; i < own.items.length; i++) {
        delete own.items[i].storageRefId;
    }  
    return own.items;
};

QuerySet.prototype.asStored = function() {
    var own = this;
    return own.items;
};

