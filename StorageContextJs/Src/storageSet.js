﻿function StorageSet(storeKey, config) {
    var
        self = this,
        set = function (items) {
            config.set(storeKey, items);
        },
        all = function() {
            return config.get(storeKey);
        };
        
    self.storageKey = storeKey;
    
    self.set = function (items) {

        if (!Array.isArray(items)) {
            throw new Error("set items should be an array.");
        }

        for (var i = 0; i < items.length; i++) {
            items[i].storageRefId = i + 1;
        }  
        set(items);
    };
    
    self.all = function () {
        return new QuerySet(all());
    };
    
    self.drop = function () {
        config.drop(storeKey);
    };
    
    self.flash = function () {
        config.flash(storeKey);
    };
    
    self.add = function (item) {
        if (_.contains(Object.keys(item), "storageRefId")) {
            throw new Error("added item shouldn't have storageRefId property.");
        }
        
        var element = _.last(all());
        var lastStorageRefId = (element == null) ? 0 : element.storageRefId;
        item.storageRefId = ++lastStorageRefId;

        var elements = all();
        elements.push(item);
        set(elements);
    };

    self.replace = function (storedItem) {
        if (!_.contains(Object.keys(storedItem), "storageRefId")) {
            throw new Error("item to replace should have storageRefId.");
        }

        var itemsToReplace = _.where(all(), { storageRefId: storedItem.storageRefId });
        if (itemsToReplace.length > 1) {
            throw new Error("found more than one stored item with same storageRefId.");
        }
        if (itemsToReplace.length === 0) {
            throw new Error("stored item with same storageRefId not found.");
        }
        
        var elements = all();
        elements = elements.map(function (obj) { return obj.storageRefId === storedItem.storageRefId ? storedItem : obj; });
        set(elements);
    };
    
    self.remove = function (storedItem) {
        var itemsToRemove = _.where(all(), { storageRefId: storedItem.storageRefId });
        if (itemsToRemove.length > 1) {
            throw new Error("found more than one stored item with same storageRefId.");
        }
        if (itemsToRemove.length === 0) {
            throw new Error("stored item with same storageRefId not found.");
        }
        
        var elements = all();
        elements = _.without(elements, _.findWhere(elements, { storageRefId: storedItem.storageRefId }));
        set(elements);
    };
}