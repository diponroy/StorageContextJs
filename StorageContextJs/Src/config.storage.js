﻿(function(storageContext) {

    storageContext.storageSetting = {
        keys: function() {
            amplify.store();
        },
        set: function(storeKey, items) {
            amplify.store(storeKey, items);
        },
        get: function(storeKey) {
            return amplify.store(storeKey);
        },
        drop: function(storeKey) {
            amplify.store(storeKey, null);
        },
        flash: function (storeKey) {
            amplify.store(storeKey, []);
        }
    };

}(storageContext));
