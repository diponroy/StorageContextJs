﻿(function() {
  
    var db = storageContext;

    QUnit.module("All", {
        beforeEach: function () {
            db.dropAndCreate();
        }
    });

    QUnit.test("All as added item", function(assert) {
        amplify.store('user', [
            { storageRefId: 1, id: 1, name: 'Han' },
            { storageRefId: 2, id: 2, name: 'Mos' }
        ]);
        var expectedItems = [
            { id: 1, name: 'Han' },
            { id: 2, name: 'Mos' }
        ];

        var items = db.user.all().asAdded();
        assert.ok(expectedItems.length === items.length);
        assert.deepEqual(items[0], expectedItems[0]);
        assert.deepEqual(items[1], expectedItems[1]);
    });
    
    QUnit.test("All as stored item", function (assert) {
        var storedItems = [
            { storageRefId: 1, id: 1, name: 'Han' },
            { storageRefId: 2, id: 2, name: 'Mos' }
        ];
        amplify.store('user', storedItems);

        var items = db.user.all().asStored();
        assert.ok(storedItems.length === items.length);
        assert.deepEqual(items[0], storedItems[0]);
        assert.deepEqual(items[1], storedItems[1]);
    });
    
    QUnit.module("Set", {
        beforeEach: function () {
            db.dropAndCreate();
        }
    });

    QUnit.test("Set all items to storage", function(assert) {
        var items = [
            { id: 1, name: 'Han' },
            { id: 2, name: 'Mos' }
        ];
        db.user.set(items);

        var expectedItems = [
            { storageRefId: 1, id: 1, name: 'Han' },
            { storageRefId: 2, id: 2, name: 'Mos' }
        ];

        var storedItems = amplify.store('user', items);
        assert.ok(storedItems.length === items.length);
        assert.deepEqual(storedItems[0], expectedItems[0]);
        assert.deepEqual(storedItems[1], expectedItems[1]);
    });

    QUnit.test("Items to set is not array, throw error", function(assert) {
        var items = { id: 1, age: 23 };
        assert.throws(
            function() {
                db.user.set(items);
            },
            function(err) {
                return err.message === "set items should be an array.";
            }
        );

    });
    
    QUnit.test("Items to set is null, throw error", function (assert) {
        var items = null;
        assert.throws(
            function () {
                db.user.set(items);
            },
            function (err) {
                return err.message === "set items should be an array.";
            }
        );

    });

}());