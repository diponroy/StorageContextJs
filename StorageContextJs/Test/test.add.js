﻿(function() {
    var db = storageContext;

    QUnit.module("Add", {
        beforeEach: function () {
            db.dropAndCreate();
        }
    });

    QUnit.test("Stored one item", function (assert) {
        var item = { name: 'Dipon', age: 25 };
        db.user.add(item);

        assert.equal(amplify.store("user").length, 1);
    });

    QUnit.test("Stored item has all fields of the added item", function (assert) {
        var item = { name: 'Dipon', age: 25 };
        db.user.add(item);

        var addedItem = amplify.store("user")[0];
        assert.ok(addedItem.storageRefId === 1);
        assert.ok(addedItem.name === item.name);
        assert.ok(addedItem.age === item.age);
    });

    QUnit.test("Added storage ref id to stored item", function (assert) {
        var item = { name: 'Dipon', age: 25 };
        db.user.add(item);

        var addedItem = amplify.store("user")[0];
        assert.equal(addedItem.storageRefId, 1);
    });

    QUnit.test("Added incremented storage ref id to stored item", function (assert) {
        var oldItems = [
            { storageRefId: 1, name: 'Dipon' },
            { storageRefId: 5, name: 'Ripon' }
        ];
        amplify.store("user", oldItems);

        var item = { name: 'Dip' };     //index would be 2
        db.user.add(item);

        var addedItem = amplify.store("user")[2];
        assert.equal(addedItem.storageRefId, 6);
    });

    QUnit.test("item has property storageRefId, throws error", function (assert) {
        var item = { storageRefId: 1, name: 'Dipon', age: 25 };

        assert.throws(
            function () {
                db.user.add(item);
            },
            function (err) {
                return err.message === "added item shouldn't have storageRefId property.";
            }
        );
    });
    
}());
