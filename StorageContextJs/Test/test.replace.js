﻿(function() {
    var db = storageContext;

    QUnit.module("Replace", {
        beforeEach: function () {
            db.dropAndCreate();
        }
    });

    QUnit.test("Replaced the stored item", function(assert) {
        amplify.store("user", [{ storageRefId: 1, id: 1, name: 'Dipon' }]);
        
        var storedItem = { storageRefId: 1, id: 2, name: 'Ripon' };
        db.user.replace(storedItem);

        var replacedItem = amplify.store("user")[0];
        assert.ok(replacedItem.storageRefId === storedItem.storageRefId);
        assert.ok(replacedItem.name === storedItem.name);
        assert.ok(replacedItem.age === storedItem.age);
    });

    QUnit.test("Stored item has no storageRefId property, throws error", function (assert) {
        amplify.store("user", [{ storageRefId: 1, id: 1, name: 'Dipon' }]);

        var storedItem = { id: 1, name: 'Dipon' };
        assert.throws(
            function () {
                db.user.replace(storedItem);
            },
            function (err) {
                return err.message === "item to replace should have storageRefId.";
            }
        );
    });
    
    QUnit.test("Not found any stored item with same storageRefId property, throws error", function (assert) {
        amplify.store("user", [{ storageRefId: 1, id: 1, name: 'Dipon' }]);

        var storedItem = { storageRefId: 2, id: 2, name: 'Ripon' };
        assert.throws(
            function () {
                db.user.replace(storedItem);
            },
            function (err) {
                return err.message === "stored item with same storageRefId not found.";
            }
        );
    });

    QUnit.test("found more than one stored item with same storageRefId property, throws error", function (assert) {
        amplify.store("user", [
            { storageRefId: 1, id: 1, name: 'Dipon' },
            { storageRefId: 1, id: 2, name: 'Dan' }
        ]);

        var storedItem = { storageRefId: 1, id: 2, name: 'Han' };
        assert.throws(
            function () {
                db.user.replace(storedItem);
            },
            function (err) {
                return err.message === "found more than one stored item with same storageRefId.";
            }
        );
    });

}());
