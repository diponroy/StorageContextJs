﻿(function() {
    var db = null;

    QUnit.module("Defined Storage", {
        beforeEach: function () {
            db = storageContext;
        }
    });

    QUnit.test("Storage setting's keys not defined, throws error", function (assert) {
        assert.throws(
            function () {
                db.storageSetting.keys();
            },
            function (err) {
                return err.message === "keys setting for storage was not set";
            }
        );
    });

    QUnit.test("Storage setting's set not defined, throws error", function (assert) {
        assert.throws(
            function () {
                db.storageSetting.set("Key", { id: 1 });
            },
            function (err) {
                return err.message === "set setting for storage was not set";
            }
        );
    });

    QUnit.test("Storage setting's get not defined, throws error", function (assert) {
        assert.throws(
            function () {
                db.storageSetting.get("Key");
            },
            function (err) {
                return err.message === "get setting for storage was not set";
            }
        );
    });

    QUnit.test("Storage setting's drop not defined, throws error", function (assert) {
        assert.throws(
            function () {
                db.storageSetting.drop("Key");
            },
            function (err) {
                return err.message === "drop setting for storage was not set";
            }
        );
    });

    QUnit.test("Storage setting's flash not defined, throws error", function (assert) {
        assert.throws(
            function () {
                db.storageSetting.flash("Key");
            },
            function (err) {
                return err.message === "flash setting for storage was not set";
            }
        );
    });

}());

