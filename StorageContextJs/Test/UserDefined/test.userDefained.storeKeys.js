﻿(function() {
    var db = null;

    QUnit.module("Defined StoreKey", {
        beforeEach: function () {
            db = storageContext;
        }
    });

    QUnit.test("StoreKeys are not defined, throws error", function (assert) {
        assert.throws(
            function () {
                db.keys();
            },
            function (err) {
                return err.message === "storeKeys to store data was not set";
            }
        );
    });
}());



