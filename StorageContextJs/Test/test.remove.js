﻿(function() {   
    var db = storageContext;

    QUnit.module("Remove", {
        beforeEach: function() {
            db.dropAndCreate();
        }
    });

    QUnit.test("Removd stored item", function(assert) {
        var itemToRemove = { storageRefId: 1, id: 1, name: 'Dipon' };
        
        amplify.store("user", [ itemToRemove ]);
        db.user.remove(itemToRemove);

        var storedItems = amplify.store("user");
        assert.ok(storedItems.length === 0);
    });
    
    QUnit.test("Not found any stored item with same storageRefId property, throws error", function (assert) {
        amplify.store("user", [{ storageRefId: 2, id: 1, name: 'Dipon' }]);

        var itemToRemove = { storageRefId: 1, id: 1, name: 'Dipon' };
        assert.throws(
            function () {
                db.user.remove(itemToRemove);
            },
            function (err) {
                return err.message === "stored item with same storageRefId not found.";
            }
        );
    });

    QUnit.test("found more than one stored item with same storageRefId property, throws error", function (assert) {
        amplify.store("user", [
            { storageRefId: 1, id: 1, name: 'Dipon' },
            { storageRefId: 1, id: 2, name: 'Han' }
        ]);

        var itemToRemove = { storageRefId: 1, id: 3, name: 'Dan' };
        assert.throws(
            function () {
                db.user.remove(itemToRemove);
            },
            function (err) {
                return err.message === "found more than one stored item with same storageRefId.";
            }
        );
    });
}());